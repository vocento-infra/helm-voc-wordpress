# VOCENTO - Helm wordpress

Este repo es un helm para levantar un wordpress en kubernetes.

Forma parte de la POC de Vocento para probar difernetes soluciones de cloud pública.

## Objetivo

Levantar de cero un wordpress, sin contenido pero configurable desde el principio. Solo fucionará con imágenes docker preparadas para esta prueba, como por ejemplo:

https://hub.docker.com/r/vocpmblanco/wordpress-poc

Se crearán deployments de kubernetes que levantará pods con dos contenedores, un httpd y un php-fpm

## Configuraciones

Las configuraciones más importantes, referentes a los valores de configuración de wordpress son:

* wordpress.db_name
* wordpress.db_user
* wordpress.db_password
* wordpress.db_host
* wordpress.uploads.nfs_server
* wordpress.uploads.nfs_path


## Ejemplos

Un ejemplo sencillo usando ingress para acceder al blog

Creamos un fichero de valores de helm como el siguiente: poc.yaml

```yaml
wordpress:
  db_name: wp_poc
  db_user: devel
  db_password: devel
  db_host: des.db.vocento.in
  uploads:
    nfs_server: vcseg1030.vocento.in
    nfs_path: /DESPRE/pre-ppllpdf/pdf

ingress:
  enabled: true
  hosts:
    - host: poc-wp.vocento.com
      paths:
        - path: /
          pathType: ImplementationSpecific

```

Si ya tenemos acceso con kubectl al cluster donde queremos desplegarlo, podremos ejecutar el siguiente comando

```bash
helm install poc-wp --namespace poc ./helm-voc-wordpress -f poc.yaml
```

### Cambios de prueba
- test 1
- test 2
- test 3
- test 4